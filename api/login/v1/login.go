package v1

import "github.com/gogf/gf/v2/frame/g"

type LoginReq struct {
	g.Meta   `path:"/login" method:"post" tags:"UserService" summary:"登录一个已存在的账户"`
	Name     string `v:"required"`
	Password string `v:"required"`
}
type LoginRes struct{}
