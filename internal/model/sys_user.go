package model

type UserCreateInput struct {
	Name      string
	Password  string
	Password2 string
}

type UserSignInInput struct {
	Name     string
	Password string
}

type UserGetInput struct {
	Name string
}

type UserUpdateInput struct {
	Id       int
	Name     string
	Password string
}

type UserDeleteInput struct {
	Id int
}
