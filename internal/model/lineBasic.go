package model

type LineBasicCreateInput struct {
	LineCode string
	LineName string
}

type LineBasicGetInput struct {
	LineCode string
	LineName string
}

type LineBasicUpdateInput struct {
	Id       int
	LineCode string
	LineName string
}

type LineBasicDeleteInput struct {
	Id int
}
