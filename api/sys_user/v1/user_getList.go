package v1

import (
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

type GetListReq struct {
	g.Meta `path:"/user/getList" method:"get" tags:"UserService" summary:"获取所有用户"`
}
type GetListRes struct {
	Result gdb.Result
}
