package cache

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/os/gcache"
	"time"
)

type (
	sGCacheUtil struct{}
)

var cache = gcache.New()

func init() {
	service.RegisterGCacheUtil(New())
}

func New() service.IGCacheUtil {
	return &sGCacheUtil{}
}

// SetWithNoExpire 设置缓存，不过期
func (s *sGCacheUtil) SetWithNoExpire(ctx context.Context, keyValue model.GCacheKeyValue) (err error) {
	err = cache.Set(ctx, keyValue.Key, keyValue.Value, 0)
	if err != nil {
		return err
	}
	return gerror.NewCodef(gcode.CodeOK, "设置缓存成功，缓存为: %s", cache)
}

// SetIfNotExist 当键名不存在时写入，设置过期时间{second}秒
func (s *sGCacheUtil) SetIfNotExist(ctx context.Context, key interface{}, value interface{}, second time.Duration) (b bool, err error) {
	b, err = gcache.SetIfNotExist(ctx, "k1", "v1", 1000*1000*1000*second)
	if err != nil {
		return false, err
	}
	return b, gerror.NewCodef(gcode.CodeOK, "缓存中不存在 Key = %s 的键值对，新增键值对{Key = %s Value = %s}", key, key, value)
}

// Get 获取缓存值
func (s *sGCacheUtil) Get(ctx context.Context, key interface{}) (value *gvar.Var, err error) {
	value, err = cache.Get(ctx, key)
	if err != nil {
		return nil, err
	}
	return value, gerror.NewCode(gcode.CodeOK, "获取缓存值成功")
}

// GetOrSet 获取指定键值，如果不存在时写入，并返回键值
func (s *sGCacheUtil) GetOrSet(ctx context.Context, key interface{}, value interface{}) (valueR *gvar.Var, err error) {
	valueR, err = gcache.GetOrSet(ctx, key, value, 0)
	if err != nil {
		return nil, err
	}
	return valueR, gerror.NewCode(gcode.CodeOK, "获取键值成功")
}

// KeyValueList 打印当前的键值对
func (s *sGCacheUtil) KeyValueList(ctx context.Context) (data map[interface{}]interface{}, err error) {
	data, err = gcache.Data(ctx)
	if err != nil {
		return nil, err
	}
	return data, gerror.NewCode(gcode.CodeOK, "获取键值对列表成功")
}

// KeyList 打印当前的键名列表
func (s *sGCacheUtil) KeyList(ctx context.Context) (keys []interface{}, err error) {
	keys, err = gcache.Keys(ctx)
	if err != nil {
		return nil, err
	}
	return keys, gerror.NewCode(gcode.CodeOK, "获取键名列表成功")
}

// ValueList 打印当前的键值列表
func (s *sGCacheUtil) ValueList(ctx context.Context) (values []interface{}, err error) {
	values, err = gcache.Values(ctx)
	if err != nil {
		return nil, err
	}
	return values, gerror.NewCode(gcode.CodeOK, "获取键值列表成功")
}

// Count 获取缓存大小
func (s *sGCacheUtil) Count(ctx context.Context) (size int, err error) {
	size, err = cache.Size(ctx)
	if err != nil {
		return -1, err
	}
	return size, gerror.NewCode(gcode.CodeOK, "获取缓存大小成功")
}

// ExistKey 缓存中是否存在指定键名
func (s *sGCacheUtil) ExistKey(ctx context.Context, key interface{}) (b bool, err error) {
	b, err = cache.Contains(ctx, key)
	if err != nil {
		return false, err
	}
	return b, gerror.NewCodef(gcode.CodeOK, "缓存中存在Key-%s", key)
}

// RemoveKey 删除并返回被删除的键值
func (s *sGCacheUtil) RemoveKey(ctx context.Context, key interface{}) (removedValue *gvar.Var, err error) {
	removedValue, err = cache.Remove(ctx, "k1")
	if err != nil {
		return nil, err
	}
	return removedValue, gerror.NewCodef(gcode.CodeOK, "已删除键值对{Key = %s Value = %s}", key, removedValue)
}

// Close 关闭缓存对象，让GC回收资源
func (s *sGCacheUtil) Close(ctx context.Context) (err error) {
	if err = cache.Close(ctx); err != nil {
		return err
	}
	return gerror.NewCode(gcode.CodeOK, "缓存对象已关闭")
}
