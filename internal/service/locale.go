// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"gf-demo/internal/model"
)

type (
	ILocale interface {
		// SetLocale 设置全局Locale
		// param: ctx-上下文 | setLocale-DTO
		SetLocale(ctx context.Context, setLocale model.SetLocale) error
		// T 使用系统当前默认语言进行翻译
		// param: ctx-上下文 | i18nKey-对应i18n文件中的key
		T(ctx context.Context, i18nKey string) (string, error)
		// Tf 使用系统当前默认语言进行翻译(i18n中含变量)
		// param: ctx-上下文 | i18nKey-对应i18n文件中的key | values-i18nKey中对应的变量
		Tf(ctx context.Context, i18nKey string, values ...interface{}) (string, error)
		// TW 使用指定语言进行翻译
		// param: ctx-上下文 | locale-语言名称(如en、zh-CN、zh-TW) | i18nKey-对应i18n文件中的key
		TW(ctx context.Context, locale string, i18nKey string) (res string, err error)
		// TF 使用指定语言进行翻译(i18n中含变量)
		// param: ctx-上下文 | locale-语言名称(如en、zh-CN、zh-TW) | i18nKey-对应i18n文件中的key | values-i18nKey中对应的变量
		TF(ctx context.Context, locale string, i18nKey string, values ...interface{}) (res string, err error)
		// WithLanguage 设置临时Locale
		// param: ctx-上下文 | locale-语言名称(如zh-CN、en、zh-TW)
		WithLanguage(ctx context.Context, locale string) (ctxR context.Context)
		// CheckLocale 判断系统是否有对应的Locale
		// param: ctx-上下文 | locale-语言名称(如en、zh-CN、zh-TW)
		CheckLocale(ctx context.Context, locale string) (ctxR context.Context, b bool, err error)
	}
)

var (
	localLocale ILocale
)

func Locale() ILocale {
	if localLocale == nil {
		panic("implement not found for interface ILocale, forgot register?")
	}
	return localLocale
}

func RegisterLocale(i ILocale) {
	localLocale = i
}
