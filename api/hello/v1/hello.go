package v1

import (
	"github.com/gogf/gf/v2/frame/g"
)

type HelloReq struct {
	g.Meta `path:"/gf-demo" tags:"Hello" method:"get" summary:"You first go-frame api"`
}
type HelloRes struct {
	g.Meta `mime:"text/html" example:"string"`
}
