package lineBasic

import (
	"context"
	"gf-demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"

	"github.com/gogf/gf/v2/errors/gerror"

	"gf-demo/api/lineBasic/v1"
)

func (c *ControllerV1) CheckLineName(ctx context.Context, req *v1.CheckLineNameReq) (*v1.CheckLineNameRes, error) {
	available, err := service.LineBasic().IsLineNameAvailable(ctx, req.LineName)
	if err != nil {
		return nil, err
	}
	res := &v1.CheckLineNameRes{
		OK: available,
	}
	err = gerror.NewCodef(gcode.CodeOK, `LineCode "%s" 可用`, req.LineName)
	if !available {
		return res, gerror.NewCodef(gcode.CodeBusinessValidationFailed, `LineName "%s" 已被使用`, req.LineName)
	}
	return res, err
}
