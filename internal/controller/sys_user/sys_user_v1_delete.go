package sys_user

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) Delete(ctx context.Context, req *v1.DeleteReq) (*v1.DeleteRes, error) {
	result, err := service.User().Delete(ctx, model.UserDeleteInput{
		Id: req.Id,
	})
	res := &v1.DeleteRes{
		Result: result,
	}
	return res, err
}
