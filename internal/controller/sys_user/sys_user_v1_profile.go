package sys_user

import (
	"context"
	"gf-demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) Profile(ctx context.Context, req *v1.ProfileReq) (res *v1.ProfileRes, err error) {
	res = &v1.ProfileRes{
		SysUser: service.User().GetProfile(ctx),
	}
	err = gerror.NewCode(gcode.CodeOK)
	return
}
