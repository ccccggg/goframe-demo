package model

type GCacheKeyValue struct {
	Key   interface{} // 键.
	Value interface{} // 值.
}
