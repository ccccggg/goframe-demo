package v1

import (
	"database/sql"
	"github.com/gogf/gf/v2/frame/g"
)

type CreateReq struct {
	g.Meta   `path:"/lineBasic/create" method:"post" tags:"LineBasicService" summary:"新增线路"`
	LineCode string `v:"required"`
	LineName string `v:"required"`
}
type CreateRes struct {
	Result sql.Result
}
