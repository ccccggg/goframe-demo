// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"database/sql"
	"gf-demo/internal/model"

	"github.com/gogf/gf/v2/database/gdb"
)

type (
	ILineBasic interface {
		Create(ctx context.Context, in model.LineBasicCreateInput) (sql.Result, error)
		Delete(ctx context.Context, in model.LineBasicDeleteInput) (sql.Result, error)
		Count(ctx context.Context) (int, error)
		Update(ctx context.Context, in model.LineBasicUpdateInput) (sql.Result, error)
		GetList(ctx context.Context) (gdb.Result, error)
		Get(ctx context.Context, in model.LineBasicGetInput) (gdb.Result, error)
		// IsLineCodeAvailable checks and returns given lineCode is available for signing up.
		IsLineCodeAvailable(ctx context.Context, lineCode string) (bool, error)
		// IsLineNameAvailable checks and returns given lineName is available for signing up.
		IsLineNameAvailable(ctx context.Context, lineName string) (bool, error)
		// IsIdAvailable checks and returns given id is available.
		IsIdAvailable(ctx context.Context, id int) (bool, error)
	}
)

var (
	localLineBasic ILineBasic
)

func LineBasic() ILineBasic {
	if localLineBasic == nil {
		panic("implement not found for interface ILineBasic, forgot register?")
	}
	return localLineBasic
}

func RegisterLineBasic(i ILineBasic) {
	localLineBasic = i
}
