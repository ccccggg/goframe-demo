package consts

import "github.com/gogf/gf/v2/i18n/gi18n"

var (
	DefaultLocale = "zh-CN"
	LocaleList    = []string{"en", "zh-CN", "zh-TW"}
	I18n          = gi18n.New()
)
