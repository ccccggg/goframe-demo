package login

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/login/v1"
)

func (c *ControllerV1) Login(ctx context.Context, req *v1.LoginReq) (*v1.LoginRes, error) {
	err := service.User().SignIn(ctx, model.UserSignInInput{
		Name:     req.Name,
		Password: req.Password,
	})
	return nil, err
}
