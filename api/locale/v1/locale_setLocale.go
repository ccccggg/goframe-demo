package v1

import (
	"github.com/gogf/gf/v2/frame/g"
)

type SetLocaleReq struct {
	g.Meta `path:"/locale/setLocale" method:"post" tags:"LocaleService" summary:"设置全局本地化"`
	Locale string `v:"required"`
}
type SetLocaleRes struct{}
