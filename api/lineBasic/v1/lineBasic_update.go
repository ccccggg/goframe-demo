package v1

import (
	"database/sql"
	"github.com/gogf/gf/v2/frame/g"
)

type UpdateReq struct {
	g.Meta   `path:"/lineBasic/update" method:"put" tags:"LineBasicService" summary:"修改线路"`
	Id       int    `v:"required"`
	LineCode string `v:"required"`
	LineName string `v:"required"`
}
type UpdateRes struct {
	Result sql.Result
}
