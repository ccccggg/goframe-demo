package sys_user

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) Create(ctx context.Context, req *v1.CreateReq) (*v1.CreateRes, error) {
	result, err := service.User().Create(ctx, model.UserCreateInput{
		Name:     req.Name,
		Password: req.Password,
	})
	res := &v1.CreateRes{
		Result: result,
	}
	return res, err
}
