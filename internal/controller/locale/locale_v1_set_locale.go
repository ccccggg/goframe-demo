package locale

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/locale/v1"
)

func (c *ControllerV1) SetLocale(ctx context.Context, req *v1.SetLocaleReq) (*v1.SetLocaleRes, error) {
	err := service.Locale().SetLocale(ctx, model.SetLocale{
		Locale: req.Locale,
	})
	return nil, err
}
