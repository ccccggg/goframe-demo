package v1

import "github.com/gogf/gf/v2/frame/g"

type CheckLineCodeReq struct {
	g.Meta   `path:"/lineBasic/check-lineCode" method:"post" tags:"LineBasicService" summary:"查重LineCode"`
	LineCode string `v:"required"`
}
type CheckLineCodeRes struct {
	OK bool `dc:"True-LineCode未被使用; False-LineCode已被使用"`
}
