package v1

import (
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

type GetReq struct {
	g.Meta   `path:"/lineBasic/get" method:"get" tags:"LineBasicService" summary:"查询线路"`
	LineCode string
	LineName string
}
type GetRes struct {
	Result gdb.Result
}
